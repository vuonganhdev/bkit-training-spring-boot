package com.training.bkit.controller;

import com.training.bkit.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
@Slf4j
public class UserController {

  @GetMapping("bkit-training-user")
  public UserDto getUserByUserNameAndFirstName(){
    return null;
  }

}
